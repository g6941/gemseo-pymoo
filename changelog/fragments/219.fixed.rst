Fix normalization/unnormalization of functions and disciplines that only contain integer variables.
