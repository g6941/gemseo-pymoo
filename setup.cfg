# setuptools settings
# See https://python-packaging-user-guide.readthedocs.io
# and https://setuptools.readthedocs.io for more details.
[metadata]
name = gemseo
author = GEMSEO developers
author_email = contact@gemseo.org
url = https://gitlab.com/gemseo
project_urls =
    Documentation = https://gemseo.readthedocs.io
    Source = https://gitlab.com/gemseo/dev/gemseo
    Tracker = https://gitlab.com/gemseo/dev/gemseo/-/issues
description = Generic Engine for Multi-disciplinary Scenarios, Exploration and Optimization
long_description = file: README.rst
long_description_content_type = text/x-rst
license = GNU Lesser General Public License v3
license_files =
    LICENSE.txt
    CREDITS.rst
classifiers =
    License :: OSI Approved :: GNU Lesser General Public License v3 (LGPLv3)
    Intended Audience :: Science/Research
    Topic :: Scientific/Engineering
    Operating System :: POSIX :: Linux
    Operating System :: MacOS
    Operating System :: Microsoft :: Windows
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3.7
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9

[options]
package_dir =
    =src
packages = find:
include_package_data = True
python_requires = >=3.7, <3.10
install_requires =
    docstring-inheritance ==1.0.0
    fastjsonschema <=2.15.3
    genson ==1.2.2
    h5py >=2.3,<=3.6.0
    importlib_metadata ; python_version=='3.7'
    jinja2 <=3.1.2
    matplotlib >=2,<=3.4.3
    networkx >=2.2,<=2.8
    numpy >=1.10,<=1.22.3
    packaging <=21.3
    pandas >=0.16,<=1.4.2
    pyxdsm <=2.2.1
    requests
    scipy >=1.1,<=1.7.3
    singledispatchmethod ; python_version=='3.7'
    tqdm >=4.41,<=4.64.0
    typing-extensions >=4,<5
    xdsmjs >=1.0.0,<=1.0.1
    xxhash ==3.0.0

[options.packages.find]
where = src

[options.package_data]
gemseo =
    algos/*/options/*.json
    core/*.json
    post/*.json
    problems/*/*.json
    problems/*/*/*.json
    problems/*/*_design_space.txt
    problems/*/*/*_design_space*.txt
    problems/dataset/iris.data
    utils/n2d3/js/*.js
    utils/n2d3/css/*.css
    utils/n2d3/n2_html.tmpl
    utils/n2d3/gemseo_logo.svg
    wrappers/icons/license.txt
    wrappers/icons/*.png
    wrappers/matlab/matlab_files/*.m

[options.extras_require]
all =
    # Graphviz requires the dot executable.
    graphviz >=0.16,<=0.20
    nlopt >=2.4.2,<=2.7.1
    # For pandas excel reader.
    openpyxl <=3.0.9
    openturns >=1.13,<=1.18
    pdfo >=1.0.0,<=1.2
    pydoe2 >=0.3.8,<=1.3.0
    pyside2 <=5.15.2.1
    scikit-learn >=0.18,<=1.0.2
    sympy >=0.7,<=1.10.1
    xlwings ==0.27.0 ; platform_system=='Windows'
test =
    pytest
    pytest-cov
    pytest-xdist
doc =
    autodocsumm
    pillow
    pyenchant
    sphinx
    sphinx_gallery
    sphinxcontrib.bibtex <2
    sphinxcontrib.plantuml
    sphinxcontrib.spelling
    sphinxcontrib.apidoc

[options.entry_points]
console_scripts =
    gemseo-study = gemseo.utils.study_analysis_cli:main
    gemseo-template-grammar-editor = gemseo.wrappers.template_grammar_editor:main [all]

[bdist_wheel]
universal = 1

# tools settings

[tool:pytest]
testpaths = tests
addopts =
    --disable-pytest-warnings
     # show extra info on xfailed, xpassed, and skipped tests
    -rxs

[coverage:run]
branch = True
source = gemseo
omit =
    # those paths are the installed ones, eventually under the env site-package
    */gemseo/third_party/*
    */gemseo/algos/opt/lib_snopt.py
    */gemseo/post/core/colormaps.py
    */gemseo/wrappers/template_grammar_editor.py
    */gemseo/wrappers/xls_discipline.py

[coverage:report]
exclude_lines =
    pragma: no cover
    raise NotImplementedError
    if __name__ == .__main__.:
    plt.show()
    pylab.plt.show()

[flake8]
# See http://www.pydocstyle.org/en/latest/error_codes.html for more details.
# https://github.com/PyCQA/flake8-bugbear#how-to-enable-opinionated-warnings
ignore =
    E501
    # no docstring for standard and private methods
    D105
    E203
    W503
    # TODO: remove if we decide to allow this API change.
    N818
select = B,C,D,E,F,G,N,T,W,B950
# settings for compatibility with black, see
# https://github.com/psf/black/blob/master/docs/compatible_configs.md#flake8
# https://black.readthedocs.io/en/stable/the_black_code_style.html?highlight=bugbear#line-length
max-line-length = 88
docstring-convention = google
per-file-ignores =
    # ignore docstring that have not yet be converted to the google style
    src/gemseo/algos/*:D
    src/gemseo/caches/*:D
    src/gemseo/core/*:D
    src/gemseo/formulations/*:D
    src/gemseo/mda/*:D
    src/gemseo/mlearning/*:D
    src/gemseo/post/*:D
    src/gemseo/problems/*:D
    src/gemseo/utils/*:D
    src/gemseo/wrappers/*:D
    src/gemseo/api.py:D
    tests/*.py:D
    # also ignore print statements violations in the examples and tutorials
    doc_src/*.py:T,D
